package test;
import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bloomnet.deliveryapi.entity.Address;
import net.bloomnet.deliveryapi.entity.Delivery;

@SuppressWarnings("deprecation")
public class PostDeliveryData{
	
	private static final String apiKey = "mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em";

	  public static void main(String[] args) throws Exception {
		///////////////////Save an image for use with data saving later on  
		MyHttpClient client = new MyHttpClient();//use this client to test production
	    @SuppressWarnings("resource")
		HttpClient client2 = new DefaultHttpClient();//use this client to test locally
	    
		@SuppressWarnings("unused")
		CloseableHttpClient httpclient = client.getNewHttpClient(false);//Staging use ONLY, accepts ALL certifcates
		//CloseableHttpClient httpclient = client.getNewHttpClient(true);//Production use

	   HttpPost httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPI/v1/SaveImage");
	    //File file = new File("C:\\102423.png");
	    
	    /*HttpEntity httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
	    		.addPart("file", new FileBody(file, ContentType.MULTIPART_FORM_DATA))
	    		.build();
	    httppost.setEntity(httpEnt);

	    System.out.println("executing request " + httppost.getRequestLine());
	    
	    HttpResponse response = client2.execute(httppost);
	    HttpEntity resEntity = response.getEntity();

	    String savedImage = "";
	    if (resEntity != null) {
	      String JSON = EntityUtils.toString(resEntity);
	      
	      if(JSON.contains("successMessage")){
	    	  savedImage = JSON.split("\"imageName\":\"")[1].split("\"")[0];
	      }else{
	    	  savedImage = "no image saved";
	      }
	      System.out.println(JSON);
	      System.out.println(savedImage);
	      EntityUtils.consume(resEntity);
	    }*/
	    //////////////////Save delivery Data
		/*httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPI/v1/InsertOrUpdateDeliveryInfo");
		HttpEntity httpEnt = MultipartEntityBuilder.create()
		    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
		    		.addPart("fulfiller", new StringBody("A2220000", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryDate", new StringBody("02/24/2023", ContentType.TEXT_PLAIN))
		    		.addPart("orderNumberBMS", new StringBody("163067112_TST", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryImageURL", new StringBody("https://storage.googleapis.com/r4m-uploaded-content/15b2d22e37a897536fdccff8e060be62.jpeg", ContentType.TEXT_PLAIN))
		    		.addPart("addressLine1", new StringBody("2041-22 JAFFREYYT ST", ContentType.TEXT_PLAIN))
		    		.addPart("city", new StringBody("WEYMOUTH", ContentType.TEXT_PLAIN))
		    		.addPart("state", new StringBody("MA", ContentType.TEXT_PLAIN))
		    		.addPart("zip", new StringBody("02188", ContentType.TEXT_PLAIN))
		    		.addPart("country", new StringBody("US", ContentType.TEXT_PLAIN))
		    		.addPart("shopAddressLine1", new StringBody("39 Walnut St", ContentType.TEXT_PLAIN))
		    		.addPart("shopCity", new StringBody("WEYMOUTH", ContentType.TEXT_PLAIN))
		    		.addPart("shopState", new StringBody("MA", ContentType.TEXT_PLAIN))
		    		.addPart("shopZip", new StringBody("02188", ContentType.TEXT_PLAIN))
		    		.addPart("shopCountry", new StringBody("US", ContentType.TEXT_PLAIN))
		    		.addPart("targetDeliveryDate", new StringBody("02/24/2023", ContentType.TEXT_PLAIN))
		    		.addPart("leftAt", new StringBody("Side Door", ContentType.TEXT_PLAIN))
		    		.addPart("merchant", new StringBody("00800000", ContentType.TEXT_PLAIN))
		    		.addPart("signatureImageURL", new StringBody("", ContentType.TEXT_PLAIN))
		    		.addPart("status", new StringBody("Delivered", ContentType.TEXT_PLAIN))
		    		//.addPart("attemptedReason", new StringBody("Could Not Find Address", ContentType.TEXT_PLAIN))
		    		.addPart("shopName", new StringBody("Mark's Test Shop", ContentType.TEXT_PLAIN))
		    		.addPart("lat", new StringBody("46.244700", ContentType.TEXT_PLAIN))
		    		.addPart("lon", new StringBody("-105.267500", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryCo", new StringBody("Route4Me", ContentType.TEXT_PLAIN))
		    		.build();
		    httppost.setEntity(httpEnt);
		    System.out.println("executing request " + httppost.getRequestLine());
		    long start = System.currentTimeMillis();
		    HttpResponse response = client2.execute(httppost);
		    long end = System.currentTimeMillis();
		    System.out.println("Execution Time: " + String.valueOf(end-start) + "ms.");
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);  */
		    
		   /*httppost = new HttpPost("http://localhost:8080/DeliveryAPI/v1/SaveOrUpdateWebhooks");
		    httpEnt = MultipartEntityBuilder.create()
		    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
		    		.addPart("shopCode", new StringBody("00800000", ContentType.TEXT_PLAIN))
		    		.addPart("url", new StringBody("fast-api.800-flowers.net/r/api/retention/selfservice/poc/SaveOrUpdateDeliveryHubOrder", ContentType.TEXT_PLAIN))
		    		.build();
		    httppost.setEntity(httpEnt);

		    System.out.println("executing request " + httppost.getRequestLine());
		    start = System.currentTimeMillis();
		    response = client2.execute(httppost);
		    end = System.currentTimeMillis();
		    System.out.println("Execution Time: " + String.valueOf(end-start) + "ms.");
		    resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);*/
		    
		    String token = "";
		    for(int ii=0; ii<1; ii++){
			    HttpPost httppost2 = new HttpPost("https://fast-api.800-flowers.net/r/api/oauth/token");
			    httppost2.addHeader("Content-Type","application/json");
			    httppost2.addHeader("Cookie","GCLB=CJrewOfXu8eazAE");
			    StringEntity params = new StringEntity("{\"guid\":\"i23n9No2RoeG2JVGYIjzo89J7HtEOuI9 ltrNkmaU6HM_umwx8SI5vM1bZzYrPfeDtVVSjKtfOqnaXCePPNqNyh7wpRgO6RGTNca1DEmpAcg8h56gscDrFA\"}");
			    httppost2.setEntity(params);
			    
			    HttpResponse response = httpclient.execute(httppost2);
			    System.out.println(response.getStatusLine());
			    HttpEntity resEntity = response.getEntity();
			    //System.out.println("Response: "+EntityUtils.toString(resEntity));
			    token = EntityUtils.toString(resEntity).split("accessToken\":\"")[1].split("\"")[0];
			    System.out.println(token);
			    EntityUtils.consume(resEntity);
		    }
		    
		    /*httppost = new HttpPost("https://fast-api.800-flowers.net/r/api/callcenter/csi/blmt-dlcf/atlas/blmtservice");
		    Delivery delivery = new Delivery();
		    delivery.setOrderNumberBLK("52675421");
		    delivery.setOrderNumberAtlas("290496862");
		    delivery.setFulfiller("41700000");
		    //delivery.setDeliveryNotes("This was submitted correctly");
		    delivery.setStatus("Delivered");
		    //delivery.setAttemptedReason("Could not find house");
		    delivery.setCreatedDate("2023-03-02 01:01:44 AM");
		    delivery.setDeliveryDate("03/03/2020");
		    delivery.setDeliveryTime("12:00AM");
		    delivery.setMerchant("00800000");
		    delivery.setTargetDeliveryDate("03/04/2023");
		    delivery.setTargetDeliveryTime("12:00PM");
		    delivery.setLeftAt("Side Door");
		    delivery.setDeliveryImageURL("http://s3.amazonaws.com/800flowers/delivery8.jpg");
		    Address recipientAddress = new Address();
		    recipientAddress.setAddressLine1("123 Test St.");
		    recipientAddress.setCity("New York");
		    recipientAddress.setState("NY");
		    recipientAddress.setCountry("USA");
		    recipientAddress.setZip("10010");
		    delivery.setDeliveryCo("Walmart");
		    delivery.setAddress(recipientAddress);
		    Address shopAddress = new Address();
		    shopAddress.setAddressLine1("123 Test St.");
		    shopAddress.setCity("New York");
		    shopAddress.setState("NY");
		    shopAddress.setCountry("USA");
		    shopAddress.setZip("10010");
		    delivery.setShopAddress(shopAddress);
		    ObjectMapper mapper = new ObjectMapper();
		    delivery.setShopName("Mark Shop Five");
		    mapper.setSerializationInclusion(Include.NON_NULL);
		    String json = mapper.writeValueAsString(delivery);
		    System.out.println(json);
		    StringEntity params = new StringEntity(json);
		    
		    httppost.setEntity(params);
		    httppost.addHeader("Authorization","Bearer " + token);
		    httppost.addHeader("Content-Type","application/json");
		    System.out.println("executing request " + httppost.getRequestLine());
		    HttpResponse response = client2.execute(httppost);
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(response.getStatusLine().getStatusCode());
		    System.out.println("response: " + EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);
		   	   
		   */ 
		   httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPIBeta/v1/InsertOrUpdateDeliveryInfoInBulk");
		     String jsonString = "[ {\"orderNumberBLK\":\"\",\"orderNumberAtlas\":\"\",\"orderNumberBMS\":\"38334473\",\"externalId\":\"\",\"orderNumberOther\":\"\",\"merchant\":\"G7000000\",\"fulfiller\":\"L2060000\",\"targetDeliveryDate\":\"03/30/2023\",\"deliveryDate\":\"03/30/2023\",\"deliveryTime\":\"07:34 PM\",\"signedBy\":\"Driver Masoud Einolyaghin\",\"deliveryNotes\":\"Door front\",\"signatureImageURL\":\"\",\"deliveryImageURL\":\"\",\"createdDate\":\"2023-03-18 03:04:16 AM\",\"driverName\":\"Masoud Einolyaghin\",\"deliveryCo\":\"Route4Me\",\"estimatedDeliveryDate\":\"07:34 PM\",\"originatingSystem\":\"MAS\",\"estimatedDeliveryTime\":\"03/30/2023\",\"deliveryCoTripID\":\"33562571_1_900EE5\",\"deliveryCoID\":\"099B3F9BE839E238C637AC9D94099593\",\"status\":\"Delivered\",\"shopName\":\"SHADIS SWEETS STUDIO\",\"address\":{\"addressLine1\":\"24462 SANTA CLARA AVE\",\"addressLine2\":\"\",\"city\":\"DANA POINT\",\"state\":\"CA\",\"zip\":\"92629\",\"country\":\"US\",\"lat\":\"33.463981\",\"lon\":\"-117.7027208\"},\"shopAddress\":{\"addressLine1\":\"27442 bonaventure Dr\",\"city\":\"laguna Niguel\",\"state\":\"CA\",\"zip\":\"92677\",\"country\":\"US\"}},"
		       + "{\"orderNumberBLK\":\"76166884\",\"merchant\":\"00800000\",\"fulfiller\":\"G1000000\",\"targetDeliveryDate\":\"10/08/2024\",\"deliveryImageURL\":\"https://imgur.com/a/tIrt278\",\"createdDate\":\"2024-10-08 04:10:41 PM\",\"shopName\":\"CONROYS MISSION VIEJO\",\"address\":{\"addressLine1\":\"411 E AVENIDA CORDOBA\",\"city\":\"SAN CLEMENTE\",\"state\":\"CA\",\"zip\":\"92672\",\"country\":\"US\"},\"shopAddress\":{\"addressLine1\":\"28442 MARGUERITE PKWY\",\"city\":\"MISSION VIEJO\",\"state\":\"CA\",\"zip\":\"92692\",\"country\":\"US\"}},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN303012112\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN65108756\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4990\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN497000000\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4960\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4950\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4940\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4930\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4920\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4910\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4900\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4890\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4880\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4870\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4860\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4850\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4840\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4830\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4820\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4810\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4800\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4790\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN29290\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"}]";

			    HttpEntity httpEnt = MultipartEntityBuilder.create()
			    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
			    		.addPart("jsonArray", new StringBody(jsonString, ContentType.TEXT_PLAIN))
			    		.build();
			    httppost.setEntity(httpEnt); 
			    
			    System.out.println("executing request " + httppost.getRequestLine());
			    long timeStart = System.currentTimeMillis();
			    HttpResponse response = client2.execute(httppost);
			    long endTime = System.currentTimeMillis();
			    System.out.println("Completed in " + String.valueOf(endTime-timeStart) +"ms");
			    HttpEntity resEntity = response.getEntity();
			    System.out.println("Response: "+EntityUtils.toString(resEntity));
			    EntityUtils.consume(resEntity); 
		
	    
	    ///////////////////Get Delivery Data
	    
	  HttpGet httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPI/v1/RetrieveDeliveryInfoByOrderNumber?apiKey="+apiKey+"&orderNumber=SPIDERMAN497000000");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	   // start = System.currentTimeMillis();
	    response = client2.execute(httpget);
	    //end = System.currentTimeMillis();
	   // System.out.println("Execution Time: " + String.valueOf(end-start-100) + "ms.");
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    httpget = new HttpGet("https://172.24.16.155/DeliveryAPI/v1/RetrieveDeliveryInfoHistoryByOrderNumber?apiKey="+apiKey+"&orderNumber=9105972_DJD");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = client2.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity); 
	    
	    /*httpget = new HttpGet("http://localhost:8080/DeliveryAPIBeta3/v1/RetrieveAddressesByDeliveryDate?apiKey="+apiKey+"&shopCode=A2220000&deliveryDate=07/27/2016");
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = client2.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    httpclient.close();*/
	    
	  }
}
