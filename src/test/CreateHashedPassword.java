package test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class CreateHashedPassword {
	public static void main(String args[]){
		long start = System.currentTimeMillis();
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String salt = "h7M).hL9B6hjlScXz!a0Av$()77ScXzZ"; //hard coded here to prevent discovery in database or file system
		String apiKey = "" + salt;
				StringBuffer sb = new StringBuffer();
				md.update((apiKey).getBytes());
				for (int xx=0; xx<10000; ++xx){
					 md.update(sb.toString().getBytes());
				     byte byteData[] = md.digest();
			        //convert the byte to hex format
			        sb = new StringBuffer();
			        for (int i = 0; i < byteData.length; i++) {
			        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			        }
				}
				
				long end = System.currentTimeMillis();
				
				long diff = end - start;
				
				System.out.println("Execution time: " + String.valueOf(diff)+"ms");
		       
		        System.out.println(sb.toString());
 
	}
}