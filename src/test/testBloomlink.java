package test;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.util.Base64Utils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bloomnet.deliveryapi.entity.Route4MeAddressJSON;
import net.bloomnet.deliveryapi.entity.Route4MeJSON;
import net.bloomnet.deliveryapi.entity.Route4MeNotesJSON;
import net.bloomnet.deliveryapi.responseobject.JSONInsertResponse;
import net.bloomnet.deliveryapi.util.MyHttpClient;

public class testBloomlink {
	
	public JSONInsertResponse testBloomlinkResponse(String json) throws ParseException, IOException{
	
		JSONInsertResponse jsonResponse = new JSONInsertResponse();
   		String externalID = "";
   		String status = "";
   		String attemptedReason = "";
   		String routeID = "";
   		String bloomlinkOrderNumber = "";
   		
   		try {
			json = URLDecoder.decode(json, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
   		if(!json.contains(":")){
   			json = "{\"" + json.replaceAll("\\&", "\",\"").replaceAll("=","\":\"") + "\"}";
   		}
   		if(json != null && !json.equals("")){
   			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
   			Route4MeJSON mappedJSON = null;
   			try {
				mappedJSON = mapper.readValue(json, new TypeReference<Route4MeJSON>(){});
			} catch (JsonParseException e) {
				e.printStackTrace();
				jsonResponse.setErrorMessage("Error parsing provided JSON.");
       			return jsonResponse;
			} catch (JsonMappingException e) {
				e.printStackTrace();
				jsonResponse.setErrorMessage("Error parsing provided JSON.");
       			return jsonResponse;
			} catch (IOException e) {
				e.printStackTrace();
				jsonResponse.setErrorMessage("Error parsing provided JSON.");
       			return jsonResponse;
			}
   			if(mappedJSON != null){
   				
   				String apiKey = mappedJSON.getRoot_owner_member_api_key();
   				   					
				routeID = mappedJSON.getRoute_id();
				String route4MeEndPoint = "https://www.route4me.com/api.v4/route.php?route_id="+routeID+"&api_key="+apiKey+"&route_path_output=Points&notes=1";
				
   				externalID = mappedJSON.getRoute_destination_id();
   					
				HttpGet get = new HttpGet(route4MeEndPoint);
				MyHttpClient httpClient = new MyHttpClient();
				CloseableHttpClient client = null;
				try {
					client = httpClient.getNewHttpClient(route4MeEndPoint);
					HttpResponse response = client.execute(get);
					HttpEntity resEntity = response.getEntity();
					Route4MeJSON mappedAddressJSON = mapper.readValue(EntityUtils.toString(resEntity), new TypeReference<Route4MeJSON>(){});
					EntityUtils.consume(resEntity);
					List<Route4MeAddressJSON> addresses = mappedAddressJSON.getAddresses();
					for(Route4MeAddressJSON address : addresses){
						if(address.getRoute_destination_id().equals(externalID)){
							String customFieldsJSON = String.valueOf(address.getCustom_fields());
							bloomlinkOrderNumber = customFieldsJSON.split("Bloomlink Order Number=")[1].split(",")[0];
							for(Route4MeNotesJSON notes : address.getNotes()){
								if(notes.getRoute_destination_id().equals(externalID) && (notes.getActivity_type().equals("dropoff") || notes.getActivity_type().equals("pickedup"))){
									status = "Delivered";
									break;
								}else if(notes.getRoute_destination_id().equals(externalID) && (notes.getActivity_type().equals("dropoff") || notes.getActivity_type().equals("noanswer") || notes.getActivity_type().equals("wrongaddressrecipient") || notes.getActivity_type().equals("wrongdelivery")|| notes.getActivity_type().equals("notfound") || notes.getActivity_type().equals("left_information") || notes.getActivity_type().equals("notpresent"))){
									status = "DeliveryAttempted";
									attemptedReason = notes.getActivity_type();
									break;
								}
							}
							break;
						}else{
							continue;
						}
					}
				} catch (HttpException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (KeyManagementException e) {
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (KeyStoreException e) {
					e.printStackTrace();
				}finally{
					try {
						client.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if(!bloomlinkOrderNumber.equals("")){
					
					String timestamp = mappedJSON.getActivity_timestamp();
					if(timestamp.contains("."))
						timestamp = timestamp.split("\\.")[0];
					long epoch = Long.parseLong( timestamp );
					Date date = new Date( epoch * 1000 );
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

					timestamp = sdf.format(date);
					bloomlinkOrderNumber = "18472661";
					if(status.equals("Delivered") || status.equals("DeliveryAttempted"))
						updateBloomlink(bloomlinkOrderNumber, timestamp, status, attemptedReason);
				}

   			}else{
   				jsonResponse.setErrorMessage("Error parsing provided JSON.");
       			return jsonResponse;
   			}
   		}else{
   			jsonResponse.setErrorMessage("Empty or null JSON provided.");
   			return jsonResponse;
   		}
   		jsonResponse.setSuccessMessage("Order Updated Successfully.");
       	return jsonResponse; 
	}
	
	private void updateBloomlink(final String bloomlinkOrderNumber, final String timestamp, final String status, final String attemptedReason){
    	
    	Thread thread = new Thread(new Runnable() {
            public void run() {
            	
            	String userFriendlyReason = "";
            	if(attemptedReason != null && !attemptedReason.equals("")){
            		if(attemptedReason.equals("noanswer"))
            			userFriendlyReason = "Recipient did not answer.";
            		else if(attemptedReason.equals("wrongaddressrecipient"))
            			userFriendlyReason = "Wrong address provided for recipient.";
            		else if(attemptedReason.equals("wrongdelivery"))
            			userFriendlyReason = "Brought an incorrect delivery to recipient.";
            		else if(attemptedReason.equals("notfound"))
            			userFriendlyReason = "Could not find recipient address as provided.";
            		else if(attemptedReason.equals("left_information"))
            			userFriendlyReason = "Left information for recipient.";
            		else if(attemptedReason.equals("notpresent"))
            			userFriendlyReason = "Recipient was not present for delivery.";
            	}
		    	String xmlDLCF = " <?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>A222</username><password></password><shopCode>A2220000</shopCode></security><errors /><messagesOnOrder><messageCount>1</messageCount><messageGenericDlcf><orderNumber>"+bloomlinkOrderNumber+"</orderNumber><dateOrderDelivered>"+timestamp+"</dateOrderDelivered><messageText>"+status+"</messageText><deliveryDetail>86</deliveryDetail></messageGenericDlcf></messagesOnOrder></foreignSystemInterface>";
		    	String xmlDLCA = " <?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>A222</username><password></password><shopCode>A2220000</shopCode></security><errors /><messagesOnOrder><messageCount>1</messageCount><messageGenericDlca><messageType>26</messageType><orderNumber>"+bloomlinkOrderNumber+"</orderNumber><deliveryAttemptedDate>"+timestamp+"</deliveryAttemptedDate><reasonForNonDelivery>94</reasonForNonDelivery><additionalReasonForNonDelivery /><redeliveryDetails><deliveryDate /><notes>Reason: "+userFriendlyReason+"</notes></redeliveryDetails></messageGenericDlca></messagesOnOrder></foreignSystemInterface>";
		    	
		    	String url = "https://qa.bloomlink.net/fsiv2/processor?func=postmessages&data=";
		    	HttpPost post = null;
		    	
		    	try {
					if(attemptedReason != null && !attemptedReason.equals(""))
						post = new HttpPost(url+URLEncoder.encode(xmlDLCA,"UTF-8"));
					else
						post = new HttpPost(url+URLEncoder.encode(xmlDLCF,"UTF-8"));
					
		    	} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				MyHttpClient httpClient = new MyHttpClient();
				CloseableHttpClient client = null;
				try {
					client = httpClient.getNewHttpClient(url);
					HttpResponse response = client.execute(post);
					HttpEntity resEntity = response.getEntity();
					EntityUtils.consume(resEntity);
				} catch (HttpException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (KeyManagementException e) {
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (KeyStoreException e) {
					e.printStackTrace();
				}finally{
					try {
						client.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
	          }
            });
    	thread.run();
    }
	
	static BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        final double rads = Math.toRadians(90);
        final double sin = Math.abs(Math.sin(rads));
        final double cos = Math.abs(Math.cos(rads));
        final int w = (int) Math.floor(scaledBI.getWidth() * cos + scaledBI.getHeight() * sin);
        final int h = (int) Math.floor(scaledBI.getHeight() * cos + scaledBI.getWidth() * sin);
        final BufferedImage rotatedImage = new BufferedImage(w, h, scaledBI.getType());
        final AffineTransform at = new AffineTransform();
        at.translate(w / 2, h / 2);
        at.rotate(rads,0, 0);
        at.translate(-scaledBI.getWidth() / 2, -scaledBI.getHeight() / 2);
        final AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        rotateOp.filter(scaledBI,rotatedImage);
        g.dispose();
        return rotatedImage;
    }
	
	
	public static void main(String args[]){try {
		  URL url2 = new URL("https://storage.googleapis.com/r4m-uploaded-content/e30543cfb6441891e195d65c3e322b7d.jpg");
		  BufferedImage image = ImageIO.read(url2);
	      BufferedImage bi = createResizedCopy(image, image.getWidth()/4, image.getHeight()/4, true);
	      ByteArrayOutputStream os = new ByteArrayOutputStream();
	      ImageIO.write(bi, "jpg", os);
	      InputStream in = new ByteArrayInputStream(os.toByteArray());
		  String text = Base64Utils.encodeToString(IOUtils.toByteArray(in));
		  System.out.println(text);
		//new testBloomlink().testBloomlinkResponse("{\"route_id\":\"0EFD928B7544704F5D8C471DC3CE1402\",\"route_destination_id\":678748718,\"member_id\":379943,\"note_id\":\"16547060\",\"note_attachment_url\":null,\"api_key\":\"74765B95D8A7302273F8E26567EDBD0D\",\"activity_type\":\"note-insert\",\"activity_id\":\"584196E9F4A6A5AAB83CA2043CCE6776\",\"activity_timestamp\":1626276549.0504,\"activity_message\":\"A note was added to route '0228C02A72E1C4542F08730B2D014FE9' from Web Interface\",\"remote_ip\":1813951282,\"day_id\":4213,\"device_type\":\"web\",\"root_owner_member_id\":\"379943\",\"activity_day_id\":4213,\"account_type_id\":\"126\",\"member_level\":\"0\",\"root_owner_member_api_key\":\"74765B95D8A7302273F8E26567EDBD0D\",\"root_owner_member_email\":\"[mailto:ddamico24@yahoo.com]ddamico24@yahoo.com\"}");
	} catch (ParseException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}}
	
}
