package net.bloomnet.deliveryapi.entity;

import java.util.List;

public class Route4MeJSON {
	
	private String member_id;
	private String route_id;
	private String route_destination_id;
	private String note_id;
	private String activity_id;
	private String activity_type;
	private String activity_timestamp;
	private String activity_message;
	private String remote_ip;
	private String day_id;
	private String device_type;
	private String root_owner_member_id;
	private String activity_day_id;
	private String member_level;
	private String root_owner_member_api_key;
	private String root_owner_member_email;
	private List<Route4MeAddressJSON> addresses;
	
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getRoute_id() {
		return route_id;
	}
	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}
	public String getRoute_destination_id() {
		return route_destination_id;
	}
	public void setRoute_destination_id(String route_destination_id) {
		this.route_destination_id = route_destination_id;
	}
	public String getNote_id() {
		return note_id;
	}
	public void setNote_id(String note_id) {
		this.note_id = note_id;
	}
	public String getActivity_id() {
		return activity_id;
	}
	public void setActivity_id(String activity_id) {
		this.activity_id = activity_id;
	}
	public String getActivity_type() {
		return activity_type;
	}
	public void setActivity_type(String activity_type) {
		this.activity_type = activity_type;
	}
	public String getActivity_timestamp() {
		return activity_timestamp;
	}
	public void setActivity_timestamp(String activity_timestamp) {
		this.activity_timestamp = activity_timestamp;
	}
	public String getActivity_message() {
		return activity_message;
	}
	public void setActivity_message(String activity_message) {
		this.activity_message = activity_message;
	}
	public String getRemote_ip() {
		return remote_ip;
	}
	public void setRemote_ip(String remote_ip) {
		this.remote_ip = remote_ip;
	}
	public String getDay_id() {
		return day_id;
	}
	public void setDay_id(String day_id) {
		this.day_id = day_id;
	}
	public String getDevice_type() {
		return device_type;
	}
	public void setDevice_type(String device_type) {
		this.device_type = device_type;
	}
	public String getRoot_owner_member_id() {
		return root_owner_member_id;
	}
	public void setRoot_owner_member_id(String root_owner_member_id) {
		this.root_owner_member_id = root_owner_member_id;
	}
	public String getActivity_day_id() {
		return activity_day_id;
	}
	public void setActivity_day_id(String activity_day_id) {
		this.activity_day_id = activity_day_id;
	}
	public String getMember_level() {
		return member_level;
	}
	public void setMember_level(String member_level) {
		this.member_level = member_level;
	}
	public String getRoot_owner_member_api_key() {
		return root_owner_member_api_key;
	}
	public void setRoot_owner_member_api_key(String root_owner_member_api_key) {
		this.root_owner_member_api_key = root_owner_member_api_key;
	}
	public String getRoot_owner_member_email() {
		return root_owner_member_email;
	}
	public void setRoot_owner_member_email(String root_owner_member_email) {
		this.root_owner_member_email = root_owner_member_email;
	}
	public List<Route4MeAddressJSON> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Route4MeAddressJSON> addresses) {
		this.addresses = addresses;
	}
}
