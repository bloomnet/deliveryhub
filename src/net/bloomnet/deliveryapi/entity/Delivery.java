
package net.bloomnet.deliveryapi.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/* (non-Javadoc)
 * This is the main data class for the API. It stores all delivery information
 */
@Document
public class Delivery {

	@Id
	@JsonIgnore
	protected String id;
	@JsonIgnore
	protected String userID;
	@Indexed
	protected String orderNumberBLK;
	@Indexed
	protected String orderNumberAtlas;
	@Indexed
	protected String orderNumberBMS;
	@Indexed
	protected String externalId;
	@Indexed
	protected String orderNumberOther;
	protected String merchant;
	protected String fulfiller;
	protected String targetDeliveryDate;
	@Indexed
	protected String targetDeliveryTime;
	protected String deliveryDate;
	protected String deliveryTime;
	protected String leftAt;
	protected String signedBy;
	protected String deliveryNotes;
	@JsonIgnore
	protected String signatureImageName;
	protected String signatureImageURL;
	@JsonIgnore
	protected String deliveryImageName;
	@Indexed
	protected String deliveryImageURL;
	@JsonIgnore
	protected String preDeliveryImageName;
	protected String preDeliveryImageURL;
	protected String createdDate;
	protected String driverName;
	protected String deliveryService;
	protected String deliveryCo;
	@JsonIgnore
	protected String modifiedDate;
	protected String estimatedDeliveryDate;
	protected String estimatedDeliveryTime;
	protected String deliveryCoTripID;
	protected String deliveryCoID;
	protected String status;
	protected String attemptedReason;
	protected String shopName;
	protected String deliveryImageBase64;
	protected String signatureImageBase64;
	protected String originatingSystem;
	@DBRef
	protected Address address;
	@DBRef
	protected Address shopAddress;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getFulfiller() {
		return fulfiller;
	}
	public void setFulfiller(String fulfiller) {
		this.fulfiller = fulfiller;
	}
	public String getOrderNumberBLK() {
		return orderNumberBLK;
	}
	public void setOrderNumberBLK(String orderNumberBLK) {
		this.orderNumberBLK = orderNumberBLK;
	}
	public String getOrderNumberAtlas() {
		return orderNumberAtlas;
	}
	public void setOrderNumberAtlas(String orderNumberAtlas) {
		this.orderNumberAtlas = orderNumberAtlas;
	}
	public String getOrderNumberBMS() {
		return orderNumberBMS;
	}
	public void setOrderNumberBMS(String orderNumberBMS) {
		this.orderNumberBMS = orderNumberBMS;
	}
	public String getOrderNumberOther() {
		return orderNumberOther;
	}
	public void setOrderNumberOther(String orderNumberOther) {
		this.orderNumberOther = orderNumberOther;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getLeftAt() {
		return leftAt;
	}
	public void setLeftAt(String leftAt) {
		this.leftAt = leftAt;
	}
	public String getSignedBy() {
		return signedBy;
	}
	public void setSignedBy(String signedBy) {
		this.signedBy = signedBy;
	}
	public String getSignatureImageName() {
		return signatureImageName;
	}
	public void setSignatureImageName(String signatureImageName) {
		this.signatureImageName = signatureImageName;
	}
	public String getSignatureImageURL() {
		return signatureImageURL;
	}
	public void setSignatureImageURL(String signatureImageURL) {
		this.signatureImageURL = signatureImageURL;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDeliveryImageName() {
		return deliveryImageName;
	}
	public void setDeliveryImageName(String deliveryImageName) {
		this.deliveryImageName = deliveryImageName;
	}
	public String getDeliveryImageURL() {
		return deliveryImageURL;
	}
	public void setDeliveryImageURL(String deliveryImageURL) {
		this.deliveryImageURL = deliveryImageURL;
	}
	public String getPreDeliveryImageName() {
		return preDeliveryImageName;
	}
	public void setPreDeliveryImageName(String preDeliveryImageName) {
		this.preDeliveryImageName = preDeliveryImageName;
	}
	public String getPreDeliveryImageURL() {
		return preDeliveryImageURL;
	}
	public void setPreDeliveryImageURL(String preDeliveryImageURL) {
		this.preDeliveryImageURL = preDeliveryImageURL;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDeliveryService() {
		return deliveryService;
	}
	public void setDeliveryService(String deliveryService) {
		this.deliveryService = deliveryService;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getDeliveryCo() {
		return deliveryCo;
	}
	public void setDeliveryCo(String deliveryCo) {
		this.deliveryCo = deliveryCo;
	}
	public String getTargetDeliveryDate() {
		return targetDeliveryDate;
	}
	public void setTargetDeliveryDate(String targetDeliveryDate) {
		this.targetDeliveryDate = targetDeliveryDate;
	}
	public String getDeliveryNotes() {
		return deliveryNotes;
	}
	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getTargetDeliveryTime() {
		return targetDeliveryTime;
	}
	public void setTargetDeliveryTime(String targetDeliveryTime) {
		this.targetDeliveryTime = targetDeliveryTime;
	}
	public String getEstimatedDeliveryDate() {
		return estimatedDeliveryDate;
	}
	public void setEstimatedDeliveryDate(String estimatedDeliveryDate) {
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}
	public String getEstimatedDeliveryTime() {
		return estimatedDeliveryTime;
	}
	public void setEstimatedDeliveryTime(String estimatedDeliveryTime) {
		this.estimatedDeliveryTime = estimatedDeliveryTime;
	}
	public String getDeliveryCoTripID() {
		return deliveryCoTripID;
	}
	public void setDeliveryCoTripID(String deliveryCoTripID) {
		this.deliveryCoTripID = deliveryCoTripID;

	}
	public String getDeliveryCoID() {
		return deliveryCoID;
	}
	public void setDeliveryCoID(String deliveryCoID) {
		this.deliveryCoID = deliveryCoID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAttemptedReason() {
		return attemptedReason;
	}
	public void setAttemptedReason(String attemptedReason) {
		this.attemptedReason = attemptedReason;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Address getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(Address shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getDeliveryImageBase64(){
		return deliveryImageBase64;	
	}
	public void setDeliveryImageBase64(String deliveryImageBase64){
		this.deliveryImageBase64 = deliveryImageBase64;
	}
	public String getSignatureImageBase64(){
		return signatureImageBase64;	
	}
	public void setSignatureImageBase64(String signatureImageBase64){
		this.signatureImageBase64 = signatureImageBase64;
	}
	public String getOriginatingSystem(){
		return originatingSystem;	
	}
	public void setOriginatingSystem(String originatingSystem){
		this.originatingSystem = originatingSystem;
	}
}
