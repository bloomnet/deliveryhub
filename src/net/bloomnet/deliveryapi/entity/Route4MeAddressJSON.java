package net.bloomnet.deliveryapi.entity;

import java.util.List;

public class Route4MeAddressJSON {
	private String route_destination_id;
	private Object custom_fields;
	private List<Route4MeNotesJSON> notes;
	public String getRoute_destination_id() {
		return route_destination_id;
	}
	public void setRoute_destination_id(String route_destination_id) {
		this.route_destination_id = route_destination_id;
	}
	public Object getCustom_fields() {
		return custom_fields;
	}
	public void setCustom_fields(Object custom_fields) {
		this.custom_fields = custom_fields;
	}
	public List<Route4MeNotesJSON> getNotes() {
		return notes;
	}
	public void setNotes(List<Route4MeNotesJSON> notes) {
		this.notes = notes;
	}
}
