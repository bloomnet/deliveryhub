package net.bloomnet.deliveryapi.entity;
import org.springframework.data.mongodb.core.mapping.Document;


/* (non-Javadoc)
 * This is the main data class for the API. It stores all delivery information
 */
@Document
public class DeliveryHistory extends Delivery {
	
	public DeliveryHistory(){}//default constructor	
			
}
