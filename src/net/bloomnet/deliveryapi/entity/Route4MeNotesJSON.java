package net.bloomnet.deliveryapi.entity;

public class Route4MeNotesJSON {
	private String route_destination_id;
	private String activity_type;
	private String upload_url;
	private String upload_type;
	private String contents;
	
	public String getRoute_destination_id() {
		return route_destination_id;
	}
	public void setRoute_destination_id(String route_destination_id) {
		this.route_destination_id = route_destination_id;
	}
	public String getActivity_type() {
		return activity_type;
	}
	public void setActivity_type(String activity_type) {
		this.activity_type = activity_type;
	}
	public String getUpload_url() {
		return upload_url;
	}
	public void setUpload_url(String upload_url) {
		this.upload_url = upload_url;
	}
	public String getUpload_type() {
		return upload_type;
	}
	public void setUpload_type(String upload_type) {
		this.upload_type = upload_type;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
}
