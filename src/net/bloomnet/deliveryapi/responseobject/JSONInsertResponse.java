package net.bloomnet.deliveryapi.responseobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/* (non-Javadoc)
 * This is the response provided by an attempt to upload delivery data. This class
 * is automatically converted and provided to the end user as a JSON response by Jackson
 */
@JsonInclude(Include.NON_EMPTY)
public class JSONInsertResponse {
	private String errorMessage;
	private String successMessage;
	private String orderNumber;
	private String imageName;
	

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
