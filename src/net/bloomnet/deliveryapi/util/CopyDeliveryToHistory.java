package net.bloomnet.deliveryapi.util;

import net.bloomnet.deliveryapi.entity.Delivery;
import net.bloomnet.deliveryapi.entity.DeliveryHistory;

public class CopyDeliveryToHistory {
	
	public static DeliveryHistory getHistoryFromDelivery(Delivery delivery){
		
		DeliveryHistory deliveryHistory = new DeliveryHistory();
		
		deliveryHistory.setAddress(delivery.getAddress());
		deliveryHistory.setCreatedDate(delivery.getCreatedDate());
		deliveryHistory.setDeliveryCo(delivery.getDeliveryCo());
		deliveryHistory.setDeliveryCoID(delivery.getDeliveryCoID());
		deliveryHistory.setDeliveryDate(delivery.getDeliveryDate());
		deliveryHistory.setDeliveryImageName(delivery.getDeliveryImageName());
		deliveryHistory.setDeliveryImageURL(delivery.getDeliveryImageURL());
		deliveryHistory.setDeliveryNotes(delivery.getDeliveryNotes());
		deliveryHistory.setDeliveryService(delivery.getDeliveryService());
		deliveryHistory.setDeliveryTime(delivery.getDeliveryTime());
		deliveryHistory.setDriverName(delivery.getDriverName());
		deliveryHistory.setEstimatedDeliveryDate(delivery.getEstimatedDeliveryDate());
		deliveryHistory.setEstimatedDeliveryTime(delivery.getEstimatedDeliveryTime());
		deliveryHistory.setExternalId(delivery.getExternalId());
		deliveryHistory.setFulfiller(delivery.getFulfiller());
		deliveryHistory.setLeftAt(delivery.getLeftAt());
		deliveryHistory.setMerchant(delivery.getMerchant());
		deliveryHistory.setModifiedDate(delivery.getModifiedDate());
		deliveryHistory.setOrderNumberAtlas(delivery.getOrderNumberAtlas());
		deliveryHistory.setOrderNumberBLK(delivery.getOrderNumberBLK());
		deliveryHistory.setOrderNumberBMS(delivery.getOrderNumberBMS());
		deliveryHistory.setOrderNumberOther(delivery.getOrderNumberOther());
		deliveryHistory.setPreDeliveryImageName(delivery.getPreDeliveryImageName());
		deliveryHistory.setPreDeliveryImageURL(delivery.getPreDeliveryImageURL());
		deliveryHistory.setSignatureImageName(delivery.getSignatureImageName());
		deliveryHistory.setSignatureImageURL(delivery.getSignatureImageURL());
		deliveryHistory.setSignedBy(delivery.getSignedBy());
		deliveryHistory.setStatus(delivery.getStatus());
		deliveryHistory.setTargetDeliveryDate(delivery.getTargetDeliveryDate());
		deliveryHistory.setTargetDeliveryTime(delivery.getTargetDeliveryTime());
		deliveryHistory.setUserID(delivery.getUserID());
		deliveryHistory.setAttemptedReason(delivery.getAttemptedReason());
		deliveryHistory.setOriginatingSystem(delivery.getOriginatingSystem());
		deliveryHistory.setShopAddress(delivery.getShopAddress());
		
		return deliveryHistory;
	}
}
