package net.bloomnet.deliveryapi.util;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.metadata.IIOMetadata;

import org.w3c.dom.Node;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

public class ImageHelper {
	
	int orientation = 1;

    private int alternateGetOrientation(InputStream in) throws Exception {
    	
    	int orientation = 1;
    	
    	try {
	        Metadata metadata = ImageMetadataReader.readMetadata(in);
	        ExifIFD0Directory exifIFD0Directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);     
        } catch (Exception ex) {}

        return orientation;
    }
    
    private void getMetaDataNodeOrientation(Node node, int level){
 
        Node child = node.getFirstChild();
        if (child == null){
            String value = node.getNodeValue();
            if (node.getNodeName().equalsIgnoreCase("ImageOrientation")){
            	if(value != null && !value.equals("")) orientation = Integer.valueOf(value);
            	else orientation = 1 ;
            	return;
            }
        }
 
        while (child != null){
            getMetaDataNodeOrientation(child, level + 1);
            child = child.getNextSibling();
        }
        String value = node.getNodeValue();
        if (node.getNodeName().equalsIgnoreCase("ImageOrientation")){
        	if(value != null && !value.equals("")) orientation = Integer.valueOf(value);
        	else orientation = 1;
        	return;
        }
    }
     
    public int getOrientation(IIOMetadata metadata){
    	
        String[] names = metadata.getMetadataFormatNames();
        int length = names.length;
        for (int i = 0; i < length; i++){
           getMetaDataNodeOrientation(metadata.getAsTree(names[i]), 3);
        }
        return orientation;
    }
    
    public BufferedImage rotateImage(BufferedImage resultImage, IIOMetadata metadata, InputStream in) {
    	try {
    		BufferedImage resultImage2 = null;
    		AffineTransform t = new AffineTransform();
    		int orientation = 1;
    		if(metadata != null) {
    			orientation = getOrientation(metadata);
    		}else {
    			orientation = alternateGetOrientation(in);
    		}
			boolean isVertical = false;		    			
			   switch (orientation) {
			    case 1:
			        break;
			    case 2: // Flip X
			        t.scale(-1.0, 1.0);
			        t.translate(-resultImage.getWidth(), 0);
			        break;
			    case 3: // PI rotation 
			        t.translate(resultImage.getWidth(), resultImage.getHeight());
			        t.rotate(Math.PI);
			        break;
			    case 4: // Flip Y
			        t.scale(1.0, -1.0);
			        t.translate(0, -resultImage.getHeight());
			        break;
			    case 5: // - PI/2 and Flip X
			        t.rotate(-Math.PI / 2);
			        t.scale(-1.0, 1.0);
			        isVertical = true;
			        break;
			    case 6: // -PI/2 and -width
			        t.translate(resultImage.getHeight(), 0);
			        t.rotate(Math.PI / 2);
			        isVertical = true;
			        break;
			    case 7: // PI/2 and Flip
			        t.scale(-1.0, 1.0);
			        t.translate(-resultImage.getHeight(), 0);
			        t.translate(0, resultImage.getWidth());
			        t.rotate(  3 * Math.PI / 2);
			        isVertical = true;
			        break;
			    case 8: // PI / 2
			        t.translate(0, resultImage.getWidth());
			        t.rotate(  3 * Math.PI / 2);
			        isVertical = true;
			        break;
			    }
			   AffineTransformOp scaleOp = new AffineTransformOp(t, AffineTransformOp.TYPE_BICUBIC);
			   
			   try {
				   
				   if(isVertical)
					   resultImage2 = new BufferedImage( resultImage.getHeight(), resultImage.getWidth(), resultImage.getType() );
				   
				   else
					   resultImage2 = new BufferedImage( resultImage.getWidth(), resultImage.getHeight(), resultImage.getType() );
				   
				   scaleOp.filter(resultImage, resultImage2);
				   
			   }catch(Exception ee) {}
			   
			   return resultImage2;
    	}catch(Exception ee) {
    		ee.printStackTrace();	
    	}
    	return null;
    }
}