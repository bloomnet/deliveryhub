package net.bloomnet.deliveryapi.util;

import com.mongodb.ConnectionString; 
import com.mongodb.client.MongoClient; 
import com.mongodb.client.MongoClients;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.data.mongodb.core.MongoTemplate; 
 
@Configuration 
public class MongoConfig { 
 
  public @Bean MongoClient mongoClient() { 
	  
	InputStream input = null;
		try {
			input = new FileInputStream("/opt/apps/properties/db.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
  	Properties props = new Properties();
      try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
 
    MongoClient mongoClient = MongoClients.create( 
      new ConnectionString("mongodb://"+props.getProperty("credentials")+"@"+props.getProperty("host")+"/"+props.getProperty("database")) 
    ); 
 
    return mongoClient; 
  } 
 
  public @Bean MongoTemplate mongoTemplate() { 
	//return new MongoTemplate(mongoClient(), "delivery_repository");
    return new MongoTemplate(mongoClient(), "delivery_repository_beta"); 
  } 
} 
