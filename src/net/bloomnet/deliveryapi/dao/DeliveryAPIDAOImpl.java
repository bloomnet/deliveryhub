package net.bloomnet.deliveryapi.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.WriteConcern;

import net.bloomnet.deliveryapi.entity.Address;
import net.bloomnet.deliveryapi.entity.Delivery;
import net.bloomnet.deliveryapi.entity.DeliveryHistory;
import net.bloomnet.deliveryapi.entity.User;
import net.bloomnet.deliveryapi.entity.Webhooks;

@Repository
public class DeliveryAPIDAOImpl implements DeliveryAPIDAO {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	/* (non-Javadoc)
	 * @see net.bloomnet.deliveryapi.dao.DeliveryAPIDA#addDelivery(net.bloomnet.deliveryapi.dbobjects.Delivery)
	 */
	@Override
	public void addDelivery(Delivery delivery) {
		if (!mongoTemplate.collectionExists(Delivery.class)) {
			mongoTemplate.createCollection(Delivery.class);
		}		
		mongoTemplate.setWriteConcern(WriteConcern.W1);
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.insert(delivery, DELIVERY_COLLECTION);
	}
	
	/* (non-Javadoc)
	 * @see net.bloomnet.deliveryapi.dao.DeliveryAPIDA#updateDelivery(net.bloomnet.deliveryapi.dbobjects.Delivery)
	 */
	@Override
	public void updateDelivery(Delivery delivery) {
		mongoTemplate.setWriteConcern(WriteConcern.W1);
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.save(delivery, DELIVERY_COLLECTION);
	}
	
	@Override
	public void addDeliveryHistory(DeliveryHistory delivery) {
		if (!mongoTemplate.collectionExists(DeliveryHistory.class)) {
			mongoTemplate.createCollection(DeliveryHistory.class);
		}		
		mongoTemplate.setWriteConcern(WriteConcern.W1);
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.insert(delivery, DELIVERY_HISTORY_COLLECTION);
	}
	
	@Override
	public void updateAddress(Address address) {
		mongoTemplate.setWriteConcern(WriteConcern.W1);
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.save(address, ADDRESS_COLLECTION);
	}
	
	/* (non-Javadoc)
	 * @see net.bloomnet.deliveryapi.dao.DeliveryAPIDA#getDeliveriesByOrderNumber(java.lang.String)
	 */
	@Override
	public List<Delivery> getDeliveriesByOrderNumber(String orderNumber) {
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.orOperator(Criteria.where("orderNumberBLK").is(orderNumber),Criteria.where("orderNumberAtlas").is(orderNumber),
				Criteria.where("orderNumberBMS").is(orderNumber),Criteria.where("orderNumberOther").is(orderNumber)));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
	}
	
	/* (non-Javadoc)
	 * @see net.bloomnet.deliveryapi.dao.DeliveryAPIDA#getDeliveriesHistoryByOrderNumber(java.lang.String)
	 */
	@Override
	public List<DeliveryHistory> getDeliveriesHistoryByOrderNumber(String orderNumber) {
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.orOperator(Criteria.where("orderNumberBLK").is(orderNumber),Criteria.where("orderNumberAtlas").is(orderNumber),
				Criteria.where("orderNumberBMS").is(orderNumber),Criteria.where("orderNumberOther").is(orderNumber)));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, DeliveryHistory.class, DELIVERY_HISTORY_COLLECTION);
	}
	
	/* (non-Javadoc)
	 * @see net.bloomnet.deliveryapi.dao.DeliveryAPIDA#getUsersByAPIKey(java.lang.String)
	 */
	@Override
	public List<User> getUsersByAPIKey(String apiKey) {
		Query query = new Query(Criteria.where("apiKey").is(apiKey));
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public void addAddress(Address address) {
		if (!mongoTemplate.collectionExists(Address.class)) {
			mongoTemplate.createCollection(Address.class);
		}		
		mongoTemplate.insert(address, ADDRESS_COLLECTION);
		
	}

	@Override
	public List<Address> getAddress(String addressLine1, String addressLine2, String city, String state, String zip, String country) {
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("city").is(city),Criteria.where("state").is(state),
				Criteria.where("addressLine1").is(addressLine1),Criteria.where("zip").is(zip)));
		return mongoTemplate.find(query, Address.class, ADDRESS_COLLECTION);
		
	}

	@Override
	public List<Delivery> getDeliveriesByMerchant(String merchant) {
		Query query = new Query(Criteria.where("merchant").is(merchant.toUpperCase()));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
	}

	@Override
	public List<Delivery> getDeliveriesByFulfiller(String fulfiller) {
		Query query = new Query(Criteria.where("fulfiller").is(fulfiller));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
	}

	@Override
	public List<Delivery> getDeliveriesByDateRange(String startD, String endD) {
		Query query = new Query(Criteria.where("deliveryDate").gte(startD).lte(endD));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
	}

	@Override
	public List<Delivery> getDeliveriesByExternalId(String externalId, String deliveryCo) {
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("externalId").is(externalId),Criteria.where("deliveryCo").is(deliveryCo)));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		return mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
	}
	
	@Override
	public List<Delivery> getDeliveriesByFulfillerAndTargetDeliveryDate(String fulfiller, String targetDeliveryDate){
		
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("fulfiller").is(fulfiller),Criteria.where("targetDeliveryDate").is(targetDeliveryDate)));
		List<Delivery> deliveries = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		
		return deliveries;
	}
	
	@Override
	public List<Delivery> getDeliveriesByTargetDeliveryDate(String targetDeliveryDate){
		
		Query query = new Query(Criteria.where("targetDeliveryDate").is(targetDeliveryDate));
		List<Delivery> deliveries = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		
		return deliveries;
	}
	
	@Override
	public List<Delivery> getDeliveriesByMerchantAndTargetDeliveryDate(String merchant, String targetDeliveryDate){
		
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("merchant").is(merchant),Criteria.where("targetDeliveryDate").is(targetDeliveryDate)));
		List<Delivery> deliveries = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		
		return deliveries;
	}
	
	@Override
	public List<Delivery> getDeliveriesByOrderNumbers(String orderNumbers){
		
		String[] orderList = orderNumbers.split(",");
		List<Delivery> deliveries = new ArrayList<Delivery>();
		
		for(int ii=0; ii<orderList.length; ++ii){
			List<Delivery> deliveryList = getDeliveriesByOrderNumber(orderList[ii]);
			if(deliveryList != null && deliveryList.size() > 0)
				deliveries.add(deliveryList.get(0));
		}
		return deliveries;
	}
	

	@Override
	public List<Delivery> getDeliveriesByExternalIDs(String orderNumbers, String deliveryCo) {
		
		String[] orderList = orderNumbers.split(",");
		List<Delivery> deliveries = new ArrayList<Delivery>();
		
		for(int ii=0; ii<orderList.length; ++ii){
			List<Delivery> deliveryList = getDeliveriesByExternalId(orderList[ii], deliveryCo);
			if(deliveryList != null && deliveryList.size() > 0)
				deliveries.add(deliveryList.get(0));
		}
		return deliveries;
	}
	
	@Override
	public List<Delivery> getDeliveriesByDeliveryImageAndCreatedDate(String startDate, String endDate){
		
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("deliveryImageURL").exists(true),Criteria.where("deliveryImageURL").ne(""),Criteria.where("createdDate").gt(startDate),Criteria.where("createdDate").lt(endDate)));
		List<Delivery> deliveries = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		
		return deliveries;
	}

	@Override
	public void addWebhooks(Webhooks webhooks) {
		if (!mongoTemplate.collectionExists(Webhooks.class)) {
			mongoTemplate.createCollection(Webhooks.class);
		}		

		mongoTemplate.insert(webhooks, WEBHOOKS_COLLECTION);
		
	}

	@Override
	public List<Webhooks> getWebhooksByShopCode(String shopCode) {
		Query query = new Query(Criteria.where("shopCode").is(shopCode));
		return mongoTemplate.find(query, Webhooks.class, WEBHOOKS_COLLECTION);
	}

	@Override
	public void updateWebhooks(Webhooks webhooks) {
		mongoTemplate.setWriteConcern(WriteConcern.W1);
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.save(webhooks, WEBHOOKS_COLLECTION);
		
	}
	
	@Override
	public List<Delivery> getMASReport(String startD, String endD) {
		List<Delivery> list = new ArrayList<Delivery>();
		List<Delivery> list2 = new ArrayList<Delivery>();
		Criteria criteria = new Criteria();
		Query query = new Query(criteria.andOperator(Criteria.where("createdDate").gte(startD).lte(endD),Criteria.where("deliveryImageURL").regex("^https:\\/\\/ik.imagekit.io")));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		list = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		criteria = new Criteria();
		query = new Query(criteria.andOperator(Criteria.where("createdDate").gte(startD).lte(endD),Criteria.where("deliveryImageURL").regex("^https:\\/\\/images.masdirect.com")));
		query.with(Sort.by(Direction.DESC,"createdDate"));
		list2 = mongoTemplate.find(query, Delivery.class, DELIVERY_COLLECTION);
		list.addAll(list2);
		return list;
		
	}
	
}
