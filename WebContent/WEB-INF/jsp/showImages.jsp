<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="net.bloomnet.deliveryapi.entity.Delivery" %>
<html> 
	<head>
	<title>Delivery Images</title>
	</head>
	
	<body> 
		  <% 
		  String errorMessage = String.valueOf(request.getAttribute("errorMessage"));
		  //if there is an error message, show that, otherwise show the data & image
		  if(errorMessage != null && !errorMessage.trim().equals("") && !errorMessage.trim().equalsIgnoreCase("null")){
		  %>
		  	<%=errorMessage %>
		  <%}else{
			  Delivery deliveryData = (Delivery)request.getAttribute("deliveryData");
			  if(deliveryData.getSignatureImageURL() == null && deliveryData.getDeliveryImageURL() == null && deliveryData.getPreDeliveryImageURL() == null){
			  %>
			  
			 	 <%="There were no images found for this order" %>
			 <%} %>
			  
			  <table>
			  <%if(deliveryData.getPreDeliveryImageURL() != null){ %>
			  		<tr><td> Pre-Delivery Image:</td></tr>
			  		<tr> 
			  			<td>
				  			<div style="width:500px; height:300px; padding-bottom: 100px;">
				  				<img width="100%" src="<%=deliveryData.getPreDeliveryImageURL() %>" />
				  				<br />
			  					<br />
				  			</div>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getSignatureImageURL() != null){ %>
			  		<tr><td> Signature Image:</td></tr>
			  		<tr> 
			  			<td>
				  			<div style="width:500px; height:300px; padding-bottom: 100px;">
				  				<img width="100%" src="<%=deliveryData.getSignatureImageURL() %>" />
				  				<br />
			  					<br />
				  			</div>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getDeliveryImageURL() != null){ %>
			  		<tr><td>Delivery Image:</td></tr>
			  		<tr>
			  			<td>
				  			<div style="width:500px; height:300px;">
				  				<img width="100%" src="<%=deliveryData.getDeliveryImageURL() %>" />
				  			</div>
			  			</td>
			  		</tr>
			  	<%} %>
			  </table>
			<%} %>
 	</body>
</html>
